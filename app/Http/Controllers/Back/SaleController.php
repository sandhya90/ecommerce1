<?php

namespace App\Http\Controllers\Back;
use App\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales = Sale::orderBy('updated_at', 'desc')->get();
        return view('back.sale.index', compact('sales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $rules=[
            'category_name' => 'required',
          'product_name' => 'required',
          'image' => 'mimes:jpeg,png,jpg|required',
          'price' => 'required|numeric',
          'discount' => 'required|numeric',

        ];
       $validator = Validator($data,$rules);

       if ($validator->fails()){
           return redirect()
                       ->back()
                       ->withErrors($validator)
                      ->withInput();
       }else{
                  $image=$data['image'];
                  $input = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = 'files';
                  $image->move($destinationPath, $input);

                $sale = new Sale();
                $sale->category_name = $request->category_name;
                $sale->product_name = $request->product_name;
                $sale->image = $input;
                $sale->price = $request->price;
                $sale->discount = $request->discount;
                $sale->save();
                return redirect()->back()->with('success', 'Product of Sale Off Stored Successfully!!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        return view('back.sale.edit', compact('sale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        $data=$request->all();
              $rules=[
                'category_name' => 'required',
                'product_name' => 'required',
                'image' => 'mimes:jpeg,png,jpg|required',
                'price' => 'required|numeric',
                'discount' => 'required|numeric'
              ];
              $validator = Validator($data,$rules);


        $sale->category_name = $request->category_name;
        $sale->product_name = $request->product_name;
        $sale->image = $request->image;
        $sale->price = $request->price;
        $sale->discount = $request->discount;


        if($request->hasFile('image'))
        {
            $image=$data['image'];
            $input = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'files';
            $image->move($destinationPath, $input);
            // Image::make($image)->resize(100,100)->save($input);
            $oldInput = $sale->image;
            $sale->image = $input;
            Storage::delete($oldInput);
        }

        $sale->save();
        return redirect()->route('sale.create')->with('success', 'Product of Sale Off Updated Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        $sale->delete();
        return redirect()->back()->with('success', 'Product of Sale Off  Deleted Successfully!!!');
    }
}
