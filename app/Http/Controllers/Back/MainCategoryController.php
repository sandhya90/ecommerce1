<?php

namespace App\Http\Controllers\Back;
use App\MainCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()                                                                                                    
    {
        $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
        return view('back.mainCategory.index', compact('maincategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required|max:30',
        ];

        $this->validate($request, $rule);

        $maincategory = new MainCategory();
        $maincategory->name = $request->name;
        $maincategory->save();
        return redirect()->back()->with('success', 'Main Category  Stored Successfully!!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MainCategory $maincategory)
    {
        return view('back.mainCategory.edit', compact('maincategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainCategory $maincategory)
    {
        $data=$request->all();
              $rules=[
                'name' => 'min:3'
              ];
              $validator = Validator($data,$rules);

        $maincategory->name = $request->name;
        $maincategory->save();
        return redirect()->route('maincategory.create')->with('success', 'Main Category Updated Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainCategory $maincategory)
    {
        $maincategory->delete();
        return redirect()->back()->with('success', 'Main Category deleted Successfully!!');
    }

    
}
