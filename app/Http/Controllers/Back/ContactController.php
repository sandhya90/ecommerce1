<?php

namespace App\Http\Controllers\Back;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contacts = Contact::orderBy('updated_at', 'desc')->paginate(3);
        return view('back.contact.index', compact('contacts'))
                                        ->with('i', (request()->input('page', 1) -1) *3);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'phone' => 'required|max:10',
            'address' => 'required',
            'opening_time' => 'required',
            'closing_time' => 'required',
            'email' => 'required|email'
        ];

        $this->validate($request, $rule);

        $contact = new Contact();
        $contact->phone = $request->phone;
        $contact->address = $request->address;
        $contact->opening_time = $request->opening_time;
        $contact->closing_time = $request->closing_time;
        $contact->email = $request->email;
        $contact->save();
        return redirect()->back()->with('success', 'Contact  Stored Successfully!!!');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('back.contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $data=$request->all();
              $rules=[
                'phone' => 'required|max:10',
                'address' => 'required',
                'opening_time' => 'required',
                'closing_time' => 'required',
                'email' => 'required|email'
              ];
              $validator = Validator($data,$rules);

              $contact->phone = $request->phone;
              $contact->address = $request->address;
              $contact->opening_time = $request->opening_time;
              $contact->closing_time = $request->closing_time;
              $contact->email = $request->email;
              $contact->save();
             return redirect()->route('contact.create')->with('success', 'Contact Updated Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect()->back()->with('success', 'Contact Deleted Successfully!!');
    }
}
