<?php

namespace App\Http\Controllers\Back;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }

    public function showAdminIndex()
    {
        return view('back.index');
    }

    public function showProfile(Admin $admin)
    {
        $admin = Auth::user();
        return view('back.admin-profile', compact('admin'));
    }

    public function updateProfile(Request $request, Admin $admin)
    {
        $data=$request->all();
        $rules=[
          'name' => 'min:3',
          'address' => 'required',
          'email' => 'required|email',
          'contact_num' =>'required|max:10',
          'password' => 'required'
        ];

        $validator = Validator($data,$rules);

        $admin->name = $request->name;
        $admin->address = $request->address;
        $admin->email = $request->email;
        $admin->contact_num = $request->contact_num;
        $admin->password = Hash::make($request->password);
        $admin->save();
        return redirect()->route('profile', compact('admin'))->with('success', 'Profile Updated Successfully!!');
    }

}
