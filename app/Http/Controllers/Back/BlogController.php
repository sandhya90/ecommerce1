<?php

namespace App\Http\Controllers\Back;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;



class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogs = Blog::orderBy('updated_at', 'desc')->get();
        return view('back.blog.index', compact('blogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
              $rules=[
                'title' => 'min:3|required',
                'slug' => 'required|min:3|alpha_dash|unique:blogs',
                'description' => 'min:10|required',
                'date' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
                'author_name' => 'required',
                'author_post' => 'required',
                'author_image' => 'mimes:jpeg,png,jpg',
                'category' => 'required',
                'tags' => 'required',
                'social_media' => 'required'
              ];
             $validator = Validator($data,$rules);

             if ($validator->fails()){
                 return redirect()
                             ->back()
                             ->withErrors($validator)
                            ->withInput();
             }else{
                        $image=$data['image'];
                        $input = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = 'files';
                        $image->move($destinationPath, $input);

                        $image2=$data['author_image'];
                        $input2 = time().'.'.$image2->getClientOriginalExtension();
                        $destinationPath2 = 'files';
                        $image2->move($destinationPath2, $input2);


                        $blog = new Blog();
                        $blog->title = $request->title;
                        $blog->slug = $request->slug;
                        $blog->description = $request->description;
                        $blog->date = $request->date;
                        $blog->image = $input;
                        $blog->author_name = $request->author_name;
                        $blog->author_post = $request->author_post;
                        $blog->author_image = $input2;
                        $blog->category = $request->category;
                        $blog->tags = $request->tags;
                        $blog->social_media = $request->social_media;
                        $blog->save();
                        return redirect()->back()->with('success', 'Blog Stored Successfully!!!');
                } 
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('back.blog.show')->withBlog($blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('back.blog.edit', compact('blog'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $data=$request->all();
              $rules=[
                'title' => 'min:3|required',
                'slug' => 'required|min:3|alpha_dash|unique:blogs',
                'description' => 'min:10|required',
                'date' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
                'author_name' => 'required',
                'author_post' => 'required',
                'author_image' => 'mimes:jpeg,png,jpg',
                'category' => 'required',
                'tags' => 'required',
                'social_media' => 'required'
              ];
              $validator = Validator($data,$rules);

              $blog->title = $request->title;
              $blog->slug = $request->slug;
              $blog->description = $request->description;
              $blog->date = $request->date;
              $blog->author_name = $request->author_name;
              $blog->author_post = $request->author_post;
              $blog->category = $request->category;
              $blog->tags = $request->tags;
              $blog->social_media = $request->social_media;

        if($request->hasFile('image'))
        {
            $image=$data['image'];
            $input = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'files';
            $image->move($destinationPath, $input);
            // Image::make($image)->resize(100,100)->save($input);
            $oldInput = $blog->image;
            $blog->image = $input;
            Storage::delete($oldInput);

            $image2=$data['author_image'];
            $input2 = time().'.'.$image2->getClientOriginalExtension();
            $destinationPath2 = 'files';
            $image2->move($destinationPath2, $input2);
            // Image::make($image)->resize(100,100)->save($input);
            $oldInput2 = $blog->author_image;
            $blog->author_image = $input2;
            Storage::delete($oldInput2);
        }

        $blog->save();
        return redirect()->route('blog.create')->with('success', 'Blog Updated Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        Storage::delete($blog->image);
        Storage::delete($blog->author_image);
        $blog->delete();
        return redirect()->back()->with('success', 'Blog Deleted Successfully!!');
    }
}
