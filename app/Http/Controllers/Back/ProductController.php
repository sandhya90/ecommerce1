<?php

namespace App\Http\Controllers\Back;
use App\Product;
use App\MainCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
        $products = Product::orderBy('updated_at', 'desc')->get();
        return view('back.Product.index', compact('maincategories', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
              $rules=[
                'product_name' => 'min:3',
                'price' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
              ];
             $validator = Validator($data,$rules);

             if ($validator->fails()){
                 return redirect()
                             ->back()
                             ->withErrors($validator)
                            ->withInput();
             }else{
                        $image=$data['image'];
                        $input = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = 'files';
                        $image->move($destinationPath, $input);
//                         Image::make($image)->resize(100,100)->save($input);


                        $product = new Product();
                        $product->maincategory_id = $request->maincategory_id;
                        $product->product_name = $request->product_name;
                        $product->price = $request->price;
                        $product->image = $input;
                        $product->save();
                        return redirect()->back()->with('success', 'Product Stored Successfully!!!');
                    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('back.Product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data=$request->all();
              $rules=[
                'product_name' => 'min:3',
                'price' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
              ];
              $validator = Validator($data,$rules);

        $product->maincategory_id = $request->maincategory_id;
        $product->product_name = $request->product_name;
        $product->price = $request->price;
        $product->image = $request->image;
              
        if($request->hasFile('image'))
        {
            $image=$data['image'];
            $input = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'files';
            $image->move($destinationPath, $input);
            // Image::make($image)->resize(100,100)->save($input);
            $oldInput = $product->image;
            $product->image = $input;
            Storage::delete($oldInput);
        }

        $product->save();
        return redirect()->route('product.create')->with('success', 'Product Updated Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->back()->with('success', 'Product Deleted Successfully!!');

    }

    public function viewProduct($id)
    {
        $products = Product::where('maincategory_id', $id)->orderBy('updated_at', 'desc')->get();
        return view('back.Product.showProduct', compact('products', 'id'));
    }

    public function showProductDetail($id)
    {
        $product = Product::find($id);
        return view('front.pages.product-details', compact('product'));
    }
}
