<?php

namespace App\Http\Controllers\Front;
use App\LeaveMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaveMessageController extends Controller
{
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ];

        $this->validate($request, $rule);

        $leavemessage = new LeaveMessage;
        $leavemessage->name = $request->name;
        $leavemessage->email = $request->email;
        $leavemessage->message = $request->message;
        $leavemessage->save();
        return redirect()->back()->with('success', 'Message Send Successfully!!!');
    }

    public function create()
    {
        $leavemessages = LeaveMessage::orderBy('updated_at', 'desc')->get();
        return view('back.leaveMessage.index', compact('leavemessages'));
    }

    public function destroy(LeaveMessage $leavemessage)
    {
        $leavemessage->delete();
        return redirect()->back()->with('success', 'Message Deleted Successfully!!!');

    }

    
}
