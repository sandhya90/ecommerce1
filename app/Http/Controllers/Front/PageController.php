<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class PageController extends Controller
{
    public function showIndex()
    {
        return view('front.pages.index');
    }

    public function showBlogDetails()
    {
        return view('front.pages.blog-details');
    }

    public function showBlog()
    {
        return view('front.pages.blog');
    }

    public function showCheckout()
    {
        return view('front.pages.checkout');
    }

    public function showContact()
    {
        return view('front.pages.contact');
    }

    public function showShopDetails()
    {
        return view('front.pages.shop-details');
    }

    public function showShopGrid()
    {
        return view('front.pages.shop-grid');
    }

    public function ShowShopingCart()
    {
        return view('front.pages.shoping-cart');
    }

    public function showSearchedProduct(Request $request)
    {
        if($request->get('searchonproduct')){
            $query = $request->get('searchonproduct');

            $searched_product = Product::where('product_name', 'like', '%'.$query.'%')->get();
           return view('front.pages.searched_product', compact('searched_product', 'query'));
        }
        
        // return redirect()->back()->with('error', 'Your Searched product not found!!!');
    }
}