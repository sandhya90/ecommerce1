<?php

namespace App\Providers;

use App\Contact;
use App\MainCategory;
use App\Product;
use App\Blog;
use App\Sale;
use Illuminate\Support\ServiceProvider;

class ViewIndexProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        

        
        view()->composer('front.pages.index', function($view){
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $products = Product::orderBy('updated_at', 'desc')->get();
            $blogs = Blog::orderBy('updated_at', 'desc')->paginate(3);
            $view->with(compact(['blogs','maincategories', 'products']))->with('i', (request()->input('page', 1) -1) * 3);

        });

        view()->composer('front.include.nav3', function($view){
            $products = Product::orderBy('updated_at', 'desc')->get();
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $contacts = Contact::orderBy('updated_at', 'desc')->first();
            $view->with(compact(['products', 'maincategories', 'contacts']));
        });
        
        view()->composer('front.include.footer', function($view){
            $contacts = Contact::orderBy('updated_at', 'desc')->first();
            $view->with(compact('contacts'));
        });

        view()->composer('front.pages.contact', function($view){
            $contacts = Contact::orderBy('updated_at', 'desc')->first();
            $view->with(compact('contacts'));
        });


        view()->composer('front.pages.shop-grid', function($view){
            $products = Product::orderBy('updated_at', 'desc')->paginate(6);
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $sales = Sale::orderBy('updated_at', 'desc')->get();
            $view->with(compact(['products', 'sales', 'maincategories']))->with('i', (request()->input('page', 1) -1) * 6);
        });

        view()->composer('front.pages.shop-details', function($view){
            $products = Product::orderBy('updated_at', 'desc')->paginate(4);
            $view->with(compact('products'))->with('i', (request()->input('page', 1) -1)*4);
        });

        view()->composer('front.pages.blog', function($view){
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $blogs = Blog::orderBy('updated_at', 'desc')->paginate(4);
            $view->with(compact(['blogs','maincategories']))->with('i', (request()->input('page', 1) -1) * 4);
        });

        view()->composer('front.pages.blog-details', function($view){
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $blogs = Blog::orderBy('updated_at', 'desc')->paginate(3);
            $view->with(compact(['blogs', 'maincategories']))->with('i', (request()->input('page', 1) -1) * 3);
        });


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
