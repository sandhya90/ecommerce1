<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    protected $fillable = ['name']; 

    public function Product()
    {
        return $this->hasMany('App\Product');
    }
}
