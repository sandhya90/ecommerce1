<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function mainCategory()
    {
        return $this->belongsTo('App\MainCategory', 'maincategory_id');
    }
}
