@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

<h3 class="text-center">List of Email</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> Email</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($newsletters as $newsletter)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$newsletter->email}}</td>


            <td>
              <a href = "{{route('newsletter.destroy', $newsletter->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>


</div>
</div>
@endsection