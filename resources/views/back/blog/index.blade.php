@extends('back.include.layout')
@section('content')

<!-- for horizontal scrollmenu -->
<style>
    div.scrollmenu {
    /* background-color: #333; */
    overflow: auto;
    white-space: nowrap;
    }

    div.scrollmenu a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px;
    text-decoration: none;
    }

    div.scrollmenu a:hover {
    background-color: #777;
    }
</style>

<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'blog.store', 'enctype' => 'multipart/form-data'])}}


<div class = "form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label>Title</label>   
    <input type="string"  name='title' value = "{{old('title')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('title') }}</small>
</div>

<div class = "form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
    <label>Slug</label>   
    <input type="string"  name='slug' value = "{{old('slug')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('slug') }}</small>
</div>

<div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label>Description</label>   
    <textarea name='description' class="form-control" required>{{old('description')}}</textarea>
    <small class="text-danger">{{ $errors->first('description') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('date') ? ' has-error' : '' }}">
    <label>Date</label>   
    <input type="date"  name='date' value = "{{old('date')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('date') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('author_name') ? ' has-error' : '' }}">
    <label>Author Name</label>   
    <input type="string"  name='author_name' value = "{{old('author_name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('author_name') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('author_post') ? ' has-error' : '' }}">
    <label>Author Post</label>   
    <input type="string"  name='author_post' value = "{{old('author_post')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('author_post') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('author_image') ? ' has-error' : '' }}">
    <label>Author Image</label>   
    <input type="file"  name='author_image' value = "{{old('author_image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('author_image') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('category') ? ' has-error' : '' }}">
    <label>Category</label>   
    <input type="string"  name='category' value = "{{old('category')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('category') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
    <label>Tags</label>   
    <input type="string"  name='tags' value = "{{old('tags')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('tags') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('social_media') ? ' has-error' : '' }}">
    <label>Social Media</label>   
    <input type="string"  name='social_media' value = "{{old('social_media')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('social_media') }}</small>
  </div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Blog</h3>

<div class="scrollmenu">
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> Title</th>
            <th scope="col"> Slug</th>
            <th scope="col"> Description</th>
            <th scope="col"> Date</th>
            <th scope="col"> Image</th>
            <th scope="col"> Author Name</th>
            <th scope="col"> Author Post</th>
            <th scope="col"> Author Image</th>
            <th scope="col"> Category</th>
            <th scope="col"> Tags</th>
            <th scope="col"> Media</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($blogs as $blog)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$blog->title}}</td>
            <td>{{$blog->slug}}</td>
            <td>{{$blog->description}}</td>
            <td>{{$blog->date}}</td>
            <td>{{$blog->image}}</td>
            <td>{{$blog->author_name}}</td>
            <td>{{$blog->author_post}}</td>
            <td>{{$blog->author_image}}</td>
            <td>{{$blog->category}}</td>
            <td>{{$blog->tags}}</td>
            <td>{{$blog->social_media}}</td>


            <td>
              <a href = "{{route('blog.edit', $blog->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('blog.destroy', $blog->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>
</div>

</div>
</div>

@endsection