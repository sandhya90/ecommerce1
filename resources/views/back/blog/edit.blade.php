@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['blog.update', $blog->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    <div class = "form-group">
        <label>Title</label>
        <input type="string"  name='title' value = "{{ old('title') ?? $blog->title }}" class="form-control @error('title') is-invalid @enderror" required><br>
        @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Slug</label>
        <input type="string"  name='slug' value = "{{ old('slug') ?? $blog->slug }}" class="form-control @error('slug') is-invalid @enderror" required><br>
        @error('slug')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Description</label>
        <textarea name='description' class="form-control @error('description') is-invalid @enderror" required>{{ old('description') ?? $blog->description }}</textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Date</label>
        <input type="date"  name='date' value = "{{ old('date') ?? $blog->date }}" class="form-control @error('date') is-invalid @enderror" required><br>
        @error('description')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Image</label>
        <input type="file"  name='image' value="{{ old('image') ?? $blog->image }}" class="form-control @error('image') is-invalid @enderror">{{ $blog->image }}<br>
        @error('image')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Author Name</label>
        <input type="string"  name='author_name' value = "{{ old('author_name') ?? $blog->author_name }}" class="form-control @error('author_name') is-invalid @enderror" required><br>
        @error('author_name')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Author Post</label>
        <input type="string"  name='author_post' value = "{{ old('author_post') ?? $blog->author_post }}" class="form-control @error('author_post') is-invalid @enderror" required><br>
        @error('author_post')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Author Image</label>
        <input type="file"  name='author_image' value = "{{ old('author_image') ?? $blog->author_image }}" class="form-control @error('author_image') is-invalid @enderror" required><br>
        @error('author_image')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Category</label>
        <input type="string"  name='category' value = "{{ old('category') ?? $blog->category }}" class="form-control @error('category') is-invalid @enderror" required><br>
        @error('category')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Tags</label>
        <input type="string"  name='tags' value = "{{ old('tags') ?? $blog->tags }}" class="form-control @error('tags') is-invalid @enderror" required><br>
        @error('tags')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Social Media</label>
        <input type="string"  name='social_media' value = "{{ old('social_media') ?? $blog->social_media }}" class="form-control @error('social_media') is-invalid @enderror" required><br>
        @error('social_media')
        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection