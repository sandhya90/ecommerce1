@extends('front.include.layout')
@section('content')
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="{{asset('files/'.$blog->image)}}" alt="">
                                </div>
                                <div class="blog__item__text">
                                    <ul>
                                        <li><i class="fa fa-calendar-o"></i> {{$blog->date}}</li>
                                        <li><i class="fa fa-comment-o"></i> 5</li>
                                    </ul>
                                    <h5><a href="#">{{$blog->title}}</a></h5>
                                    <p>{{$blog->description}}</p>
                                </div>
                            </div>

@endsection