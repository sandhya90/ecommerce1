@extends('back.include.layout')
@section('content')

<!-- for horizontal scrollmenu -->
<style>
    div.scrollmenu {
    /* background-color: #333; */
    overflow: auto;
    white-space: nowrap;
    }

    div.scrollmenu a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px;
    text-decoration: none;
    }

    div.scrollmenu a:hover {
    background-color: #777;
    }
</style>

<div class = "container mt-5">

<h3 class="text-center">List of Order</h3>
<div class="scrollmenu">

<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> First_name</th>
            <th scope="col"> Last_name</th>
            <th scope="col"> Street</th>
            <th scope="col"> House</th>
            <th scope="col"> City</th>
            <th scope="col"> Country</th>
            <th scope="col"> Phone</th>
            <th scope="col"> Email</th>
            <th scope="col"> Note</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($orders as $order)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$order->first_name}}</td>
            <td>{{$order->last_name}}</td>
            <td>{{$order->street}}</td>
            <td>{{$order->house}}</td>
            <td>{{$order->city}}</td>
            <td>{{$order->country}}</td>
            <td>{{$order->phone}}</td>
            <td>{{$order->email}}</td>
            <td>{{$order->note}}</td>
            <td>
              <a href = "{{route('order.destroy', $order->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>
</div>

</div>
</div>

<div class="col-lg-4 col-md-6">
                            <div class="checkout__order">
                                <h4>Your Order</h4>
                                <div class="checkout__order__products">Products <span>Total</span></div>
                                <?php $total = 0 ?>
                                @if(session('cart'))
                                    @foreach(session('cart') as $id => $details)
                                        <?php $total += $details['price'] * $details['quantity'] ?>
                                        <ul>
                                            <li>{{$details['name']}} <span>Rs.{{$details['price'] * $details['quantity']}}</span></li>
                                        </ul>
                                     @endforeach
                                @endif  
                                <div class="checkout__order__subtotal">Subtotal <span>Rs.{{$total}}</span></div>
                                <div class="checkout__order__total">Total <span>Rs.{{$total}}</span></div>                         
           
                                
                            </div>
</div>
@endsection