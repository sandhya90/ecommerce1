@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'sale.store', 'enctype' => 'multipart/form-data'])}}

<div class = "form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
    <label>Category Name</label>   
    <input type="string"  name='category_name' value = "{{old('category_name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('category_name') }}</small>
</div>

<div class = "form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
    <label>Product Name</label>   
    <input type="string"  name='product_name' value = "{{old('product_name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('product_name') }}</small>
</div>

<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
  </div>

<div class = "form-group{{ $errors->has('price') ? ' has-error' : '' }}">
    <label>Price</label>   
    <input type="integer"  name='price' value = "{{old('price')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('price') }}</small>
</div>

<div class = "form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
    <label>Discount</label>   
    <input type="integer"  name='discount' value = "{{old('discount')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('discount') }}</small>
</div>


<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Product</h3>

<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Category Name</th>
            <th scope="col">Product Name</th>
            <th scope="col">Image</th>
            <th scope="col">Price</th>
            <th scope="col">Discount</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($sales as $sale)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$sale->category_name}}</td>
            <td>{{$sale->product_name}}</td>
            <td>{{$sale->image}}</td>
            <td>Rs.{{$sale->price}}</td>
            <td>{{$sale->discount}}%</td>
            <td>
              <a href = "{{route('sale.edit', $sale->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('sale.destroy', $sale->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>
</div>
</div>


@endsection