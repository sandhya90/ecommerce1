@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['sale.update', $sale->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label> Category Name</label>
        <input type="string"  name='category_name' value = "{{ old('category_name') ?? $sale->category_name }}" class="form-control @error('category_name') is-invalid @enderror" required><br>
        @error('category_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Product Name</label>
        <input type="string"  name='product_name' value = "{{ old('product_name') ?? $sale->product_name }}" class="form-control @error('product_name') is-invalid @enderror" required><br>
        @error('product_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Image</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $sale->image }}" class="form-control @error('image') is-invalid @enderror" required><br>
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Price</label>
        <input type="integer"  name='price' value = "Rs.{{ old('price') ?? $sale->price }}" class="form-control @error('price') is-invalid @enderror" required><br>
        @error('price')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>discount

    <div class = "form-group">
        <label> Discount</label>
        <input type="integer"  name='discount' value = "{{ old('discount') ?? $sale->discount }}%" class="form-control @error('discount') is-invalid @enderror" required><br>
        @error('discount')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection