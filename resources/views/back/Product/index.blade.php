@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'product.store', 'enctype' => 'multipart/form-data'])}}

<div class="form-group{{ $errors->has('maincategory_id') ? ' has-error' : '' }}">
    <label for = "maincategory_id">Category</label>
    <select name="maincategory_id" id = "maincategory_id" class="form-control" required="required">
        <option value="">----select Category----</option>
        @foreach($maincategories as $maincategory)
            <option value = "{{ $maincategory->id }}"  {{ (old("maincategory_id") == $maincategory->id ? "selected":"") }}>{{$maincategory->name}}</option>
        @endforeach
    </select>
    <small class="text-danger">{{ $errors->first('maincategory_id') }}</small>
</div>

<div class = "form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
    <label>Product Name</label>   
    <input type="string"  name='product_name' value = "{{old('product_name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('product_name') }}</small>
</div>

<div class = "form-group{{ $errors->has('price') ? ' has-error' : '' }}">
    <label>Price</label>   
    <input type="decimal"  name='price' value = "{{old('price')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('price') }}</small>
</div>

<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
  </div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Product</h3>

<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Category Name</th>
            <th scope="col">Product Name</th>
            <th scope="col">Price</th>
            <th scope="col">Image</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($products as $product)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$product->mainCategory->name}}</td>
            <td>{{$product->product_name}}</td>
            <td>Rs.{{$product->price}}</td>
            <td>{{$product->image}}</td>
            <td>
              <a href = "{{route('product.edit', $product->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('product.destroy', $product->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>
</div>
</div>


@endsection