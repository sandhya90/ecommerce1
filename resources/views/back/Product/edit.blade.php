@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['product.update', $product->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    <div class = "form-group">
        <label> Category Id</label>
        <input type="string"  name='maincategory_id' value = "{{ old('maincategory_id') ?? $product->maincategory_id }}" class="form-control @error('maincategory_id') is-invalid @enderror" required><br>
        @error('maincategory_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Product Name</label>
        <input type="string"  name='product_name' value = "{{ old('product_name') ?? $product->product_name }}" class="form-control @error('product_name') is-invalid @enderror" required><br>
        @error('product_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Price</label>
        <input type="decimal"  name='price' value = "Rs.{{ old('price') ?? $product->price }}" class="form-control @error('price') is-invalid @enderror" required><br>
        @error('price')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Image</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $product->image }}" class="form-control @error('image') is-invalid @enderror" required><br>
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection