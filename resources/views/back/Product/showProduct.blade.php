@extends('front.include.layout')
@section('content')


<section class="featured spad">
    <div class="container">
        <div class="row featured__filter">
            @if(count($products) == '0')
                <div class="col-md-12" align="center">
                    <h2 align="center" style="color:red">No Products Found Under {{$id}} Category ID!!! </h2>
                </div>           
            @else
                @foreach($products as $product)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix vegetables fastfood">
                        <div class="featured__item">
                            <div class="featured__item__pic set-bg" data-setbg="{{asset('files/'.$product->image)}}">
                                <ul class="featured__item__pic__hover">
                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                    <li><a href="{{url('user/add-to-cart/'. $product->id)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul>
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="{{url('user/product-details', $product->id)}}">{{$product->product_name}}</a></h6>
                                <h5>Rs.{{$product->price}}</h5>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        
        </div>
    </div>
</section>

@endsection