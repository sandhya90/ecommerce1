@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['maincategory.update', $maincategory->id ], 'enctype' => 'multipart/form-data', 'files' => true])}}
    <div class = "form-group">
        <label> Main Category Name</label>
        <input type="string"  name='name' value = "{{ old('name') ?? $maincategory->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection