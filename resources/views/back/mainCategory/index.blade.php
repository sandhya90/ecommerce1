@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'maincategory.store', 'enctype' => 'multipart/form-data'])}}
<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Main Category Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('maincategory') }}</small>
</div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Main Category</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($maincategories as $maincategory)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$maincategory->name}}</td>

            <td>
              <a href = "{{route('maincategory.edit', $maincategory->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('maincategory.destroy', $maincategory->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection