@extends('back.include.layout')
@section('content')

<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'contact.store', 'enctype' => 'multipart/form-data'])}}


<div class = "form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label>Phone Number</label>   
    <input type="string"  name='phone' value = "{{old('phone')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('phone') }}</small>
</div>

<div class = "form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label>Address</label>   
    <input type="string"  name='address' value = "{{old('address')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('address') }}</small>
</div>

<div class = "form-group{{ $errors->has('opening_time') ? ' has-error' : '' }}">
    <label>Opening Time</label>   
    <input type="time"  name='opening_time' value = "{{old('opening_time')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('opening_time') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('closing_time') ? ' has-error' : '' }}">
    <label>Closing Time</label>   
    <input type="time"  name='closing_time' value = "{{old('closing_time')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('closing_time') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label>Email</label>   
    <input type="email"  name='email' value = "{{old('email')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('email') }}</small>
  </div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Contact</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> Phone</th>
            <th scope="col"> Adress</th>
            <th scope="col"> Opening Time</th>
            <th scope="col"> Closing Time</th>
            <th scope="col"> Email</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($contacts as $contact)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$contact->phone}}</td>
            <td>{{$contact->address}}</td>
            <td>{{$contact->opening_time}}</td>
            <td>{{$contact->closing_time}}</td>
            <td>{{$contact->email}}</td>

            <td>
              <a href = "{{route('contact.edit', $contact->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('contact.destroy', $contact->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>


</div>
</div>
  {{ $contacts->links() }}

@endsection