@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['contact.update', $contact->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    <div class = "form-group">
        <label> Phone</label>
        <input type="string"  name='phone' value = "{{ old('phone') ?? $contact->phone }}" class="form-control @error('phone') is-invalid @enderror" required><br>
        @error('phone')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Address</label>
        <input type="string"  name='address' value = "{{ old('address') ?? $contact->address }}" class="form-control @error('address') is-invalid @enderror" required><br>
        @error('address')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Opening Time</label>
        <input type="time"  name='opening_time' value = "{{ old('opening_time') ?? $contact->opening_time }}" class="form-control @error('opening_time') is-invalid @enderror" required><br>
        @error('opening_time')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Closing Time</label>
        <input type="time"  name='closing_time' value = "{{ old('closing_time') ?? $contact->closing_time }}" class="form-control @error('closing_time') is-invalid @enderror" required><br>
        @error('closing_time')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Email</label>
        <input type="email"  name='email' value = "{{ old('email') ?? $contact->email }}" class="form-control @error('email') is-invalid @enderror" required><br>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection