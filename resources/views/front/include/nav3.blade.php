

    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>All Category</span>
                        </div>
                        @foreach($maincategories as $maincategory)
                            <ul>
                                <li><a href="{{url('user/'.'category/'. $maincategory->id)}}">{{$maincategory->name}}</a></li>
                            </ul>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="{{url('/user/product/search')}}" method="POST">
                            @csrf()
                                <div class="hero__search__categories">
                                    All Categories
                                    <span class="arrow_carrot-down"></span>
                                </div>
                                <input type="text" name="searchonproduct" id="prodList" placeholder="What do yo u need?" required = "true">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>{{$contacts->phone}}</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="{{asset('img/hero/banner.jpg')}}">
                        <div class="hero__text">
                            <span>FRUIT FRESH</span>
                            <h2>Vegetable <br />100% Organic</h2>
                            <p>Free Pickup and Delivery Available</p>
                            <a href="#" class="primary-btn">SHOP NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <script>
    $(function(){
            var source = [

                @foreach($products as $product)
                {
                    value:"{{$product->product_name}}",
                    label:"{{$product->product_name}}"
                },
                @endforeach
       
            ];

        $("#prodList").autocomplete({
            source: source,
            // select: function(event, ui){
            //     window.location.href = ui.item.value;
            // }

        });
    });
  
</script>