@include('front.include.head')
@include('front.include.nav1')
@include('front.include.nav2')
@include('front.include.nav3')


<div class="page-wrapper p-t-70">
        <div class="container-fluid">
        @if(session()->has('success'))
            <div class="alert alert-success">
            <p>{{ session()->get('success') }}</p>
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
            <p>{{ session()->get('error') }}</p>
            </div>
        @endif

        @yield('content')
        </div>
</div>


@include('front.include.footer')
@include('front.include.js')