<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//for Socialite(API)
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

//Front
Route::middleware(['user'])->prefix('user')->group(function () {

    Route::get('/', 'Front\PageController@showIndex')->name('user.dashboard');
    Route::get('/blog-details', 'Front\PageController@showBlogDetails')->name('blog-details');
    Route::get('/blog', 'Front\PageController@showBlog')->name('blog');
    Route::get('/checkout', 'Front\PageController@showCheckout')->name('checkout');
    Route::get('/contact', 'Front\PageController@showContact')->name('contact');
    Route::get('/shop-details', 'Front\PageController@showShopDetails')->name('shop-details');
    Route::get('/shop-grid', 'Front\PageController@showShopGrid')->name('shop-grid');
    Route::get('/shoping-cart', 'Front\PageController@showShopingCart')->name('shoping-cart');

    //Show Specific Product
    Route::get('/category/{id}', 'Back\ProductController@viewProduct');

    //Leave Message
    Route::post('/store-message', 'Front\LeaveMessageController@store')->name('message.store');

    //Slug
    Route::get('/{blog}', 'Back\BlogController@show')->name('blog.show');

    //Newsletter
    Route::post('/store-newsletter', 'Front\NewsletterController@store')->name('newsletter.store');

    //Search Functionality
    Route::post('/product/search', 'Front\PageController@showSearchedProduct');

    //Cart
    Route::get('/add-to-cart/{id}', 'Front\CartController@addToCart');
    Route::patch('/update-cart', 'Front\CartController@update');
    Route::delete('/remove-from-cart', 'Front\CartController@remove');
    Route::post('/store-order', 'Front\CartController@storeOrder')->name('order.store');

    //Product-details
    Route::get('/product-details/{id}', 'Back\ProductController@showProductDetail')->name('product-details');

    
});



//Back
Route::middleware(['admin'])->prefix('admin')->group(function(){

    //Login
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'Back\PageController@showAdminIndex')->name('admin.dashboard');

    //Profile
    Route::get('/profile', 'Back\PageController@showProfile')->name('profile');
    Route::patch('/profile/{admin}', 'Back\PageController@updateProfile')->name('profile.update');


    //Main Category
    Route::get('/create-maincategory', 'Back\MainCategoryController@create')->name('maincategory.create');
    Route::post('/store-maincategory', 'Back\MainCategoryController@store')->name('maincategory.store');
    Route::get('/edit-maincategory/{maincategory}', 'Back\MainCategoryController@edit')->name('maincategory.edit');
    Route::patch('/update-maincategory/{maincategory}', 'Back\MainCategoryController@update')->name('maincategory.update');
    Route::get('/destroy-maincategory/{maincategory}', 'Back\MainCategoryController@destroy')->name('maincategory.destroy');

    //Product
    Route::get('/create-product', 'Back\ProductController@create')->name('product.create');
    Route::post('/store-product', 'Back\ProductController@store')->name('product.store');
    Route::get('/edit-product/{product}', 'Back\ProductController@edit')->name('product.edit');
    Route::patch('/update-product/{product}', 'Back\ProductController@update')->name('product.update');
    Route::get('/destroy-product/{product}', 'Back\ProductController@destroy')->name('product.destroy');

    //Leave Message
    Route::get('/destroy-message/{leavemessage}', 'Front\LeaveMessageController@destroy')->name('message.destroy');
    Route::get('/create-message', 'Front\LeaveMessageController@create')->name('message.create');

    //Contact Info
    Route::get('/create-contact', 'Back\ContactController@create')->name('contact.create');
    Route::post('/store-contact', 'Back\ContactController@store')->name('contact.store');
    Route::get('/edit-contact/{contact}', 'Back\ContactController@edit')->name('contact.edit');
    Route::patch('/update-contact/{contact}', 'Back\ContactController@update')->name('contact.update');
    Route::get('/destroy-contact/{contact}', 'Back\ContactController@destroy')->name('contact.destroy');

    //Blog
    Route::get('/create-blog', 'Back\BlogController@create')->name('blog.create');
    Route::post('/store-blog', 'Back\BlogController@store')->name('blog.store');
    Route::get('/edit-blog/{blog}', 'Back\BlogController@edit')->name('blog.edit');
    Route::patch('/update-blog/{blog}', 'Back\BlogController@update')->name('blog.update');
    Route::get('/destroy-blog/{blog}', 'Back\BlogController@destroy')->name('blog.destroy');

    //Newsletter
    Route::get('/create-newsletter', 'Front\NewsletterController@create')->name('newsletter.create');
    Route::get('/destroy-newsletter/{newsletter}', 'Front\NewsletterController@destroy')->name('newsletter.destroy');

    //Sale off
    Route::get('/create-sale', 'Back\SaleController@create')->name('sale.create');
    Route::post('/store-sale', 'Back\SaleController@store')->name('sale.store');
    Route::get('/edit-sale/{sale}', 'Back\SaleController@edit')->name('sale.edit');
    Route::patch('/update-sale/{sale}', 'Back\SaleController@update')->name('sale.update');
    Route::get('/destroy-sale/{sale}', 'Back\SaleController@destroy')->name('sale.destroy');

    //Cart
    Route::get('/create-order', 'Front\CartController@create')->name('order.create');
    Route::get('/destroy-order/{order}', 'Front\CartController@destroy')->name('order.destroy');

});








