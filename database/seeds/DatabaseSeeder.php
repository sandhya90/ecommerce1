<?php

use Illuminate\Database\Seeder;
use App\Admin;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('AdminTableSeeder');

        $this->command->info('admin table seeded!');
    }
}

    class AdminTableSeeder extends Seeder {

        public function run()
        {
            DB::table('admins')->delete();
    
            Admin::create([
                'name' => 'Karan Khanal',
                'email' => 'cskarankhanal@gmail.com',
                'address' => 'Baneshwor',
                'contact_num' => '9857071983',
                'password' => Hash::make('password')]);
        }
    
    }
    

